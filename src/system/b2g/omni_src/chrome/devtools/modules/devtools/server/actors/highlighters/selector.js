"use strict";const{isNodeValid}=require("./utils/markup");const{BoxModelHighlighter}=require("./box-model");
const MAX_HIGHLIGHTED_ELEMENTS=100;function SelectorHighlighter(highlighterEnv){this.highlighterEnv=highlighterEnv;this._highlighters=[];}
SelectorHighlighter.prototype={typeName:"SelectorHighlighter",show:function(node,options={}){this.hide();if(!isNodeValid(node)||!options.selector){return false;}
let nodes=[];try{nodes=[...node.ownerDocument.querySelectorAll(options.selector)];}catch(e){
}
delete options.selector;let i=0;for(let matchingNode of nodes){if(i>=MAX_HIGHLIGHTED_ELEMENTS){break;}
let highlighter=new BoxModelHighlighter(this.highlighterEnv);if(options.fill){highlighter.regionFill[options.region||"border"]=options.fill;}
highlighter.show(matchingNode,options);this._highlighters.push(highlighter);i++;}
return true;},hide:function(){for(let highlighter of this._highlighters){highlighter.destroy();}
this._highlighters=[];},destroy:function(){this.hide();this.highlighterEnv=null;}};exports.SelectorHighlighter=SelectorHighlighter;