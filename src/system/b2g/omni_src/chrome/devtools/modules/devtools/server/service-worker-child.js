"use strict";let{classes:Cc,interfaces:Ci,utils:Cu}=Components;let swm=Cc["@mozilla.org/serviceworkers/manager;1"].getService(Ci.nsIServiceWorkerManager);addMessageListener("serviceWorkerRegistration:start",message=>{let{data}=message;let array=swm.getAllRegistrations();for(let i=0;i<array.length;i++){let registration=array.queryElementAt(i,Ci.nsIServiceWorkerRegistrationInfo);



if(registration.scope===data.scope&&registration.activeWorker){
registration.activeWorker.attachDebugger();registration.activeWorker.detachDebugger();return;}}});