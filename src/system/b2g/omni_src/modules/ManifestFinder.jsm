"use strict";const{utils:Cu}=Components;Cu.import("resource://gre/modules/PromiseMessage.jsm");Cu.import("resource://gre/modules/Task.jsm");this.ManifestFinder={ contentHasManifestLink(aContent){if(!aContent||isXULBrowser(aContent)){throw new TypeError("Invalid input.");}
return checkForManifest(aContent);},browserHasManifestLink:Task.async(function*(aBrowser){if(!isXULBrowser(aBrowser)){throw new TypeError("Invalid input.");}
const msgKey="DOM:WebManifest:hasManifestLink";const mm=aBrowser.messageManager;const reply=yield PromiseMessage.send(mm,msgKey);return reply.data.result;})};function isXULBrowser(aBrowser){if(!aBrowser||!aBrowser.namespaceURI||!aBrowser.localName){return false;}
const XUL="http://www.mozilla.org/keymaster/gatekeeper/there.is.only.xul";return(aBrowser.namespaceURI===XUL&&aBrowser.localName==="browser");}
function checkForManifest(aWindow){if(!aWindow||aWindow.top!==aWindow){return false;}
const elem=aWindow.document.querySelector("link[rel~='manifest']");if(!elem||!elem.getAttribute("href")){return false;}
return true;}
this.EXPORTED_SYMBOLS=["ManifestFinder"];