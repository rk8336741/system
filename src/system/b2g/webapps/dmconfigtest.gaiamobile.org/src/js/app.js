'use strict';

//only support these keys because key "Back"&"Message" are too easy to be mis-pressed
var supportedKeyCode = [
	'Enter',
	'ArrowUp',
	'ArrowDown',
	'ArrowLeft',
	'ArrowRight',
	'Backspace'
	];

//use these variables and initMovements to process movements
var elementsTable = [];
var activeElement = "selectNodeId";

//default omaNodes for non SPR/ATT projects
var omaNodes =(function(){
	function getNodeIds() {
		return [];
	};
	function getServerIds() {
		return [];
	};
	return {
		getNodeIds: getNodeIds,
		getServerIds: getServerIds
	};
})();

function $(id) {
  return document.getElementById(id);
}

function getKeyCode(key) {
  for(var i=0; i<supportedKeyCode.length; i++) {
  	if (supportedKeyCode[i] == key)
  		return i;
  }
  return -1;
}

function onLoad() {
	
	var operatorName = navigator.jrdExtension.readRoValue("ro.operator.name");
	dump("zhouling gaia : ---onLoad, operatorName = "+operatorName);
	if (operatorName === "ATT")
	{omaNodes = omaNodes_ATT;}
	else
	{}

	initMovements();
	initActions();
	initSelects();
	initFocus();
}

function initFocus() {
	activeElement = "selectNodeId";
	$(activeElement).className="active";
}

function initMovements() {
	elementsTable = [];
	elementsTable["selectNodeId"] = {up:null, down:"inputNodeValue", left:null, right:"inputNodeValue"};
	elementsTable["inputNodeValue"] = {up:"selectNodeId", down:"buttonGetNode", left:"selectNodeId", right:"buttonGetNode"};
	elementsTable["buttonGetNode"] = {up:"inputNodeValue", down:"selectServerId", left:"inputNodeValue", right:"buttonSetNode"};
	elementsTable["buttonSetNode"] = {up:"inputNodeValue", down:"buttonSetServer", left:"buttonGetNode", right:"selectServerId"};

	elementsTable["selectServerId"] = {up:"buttonGetNode", down:null, left:"buttonSetNode", right:"buttonSetServer"};
	elementsTable["buttonSetServer"] = {up:"buttonSetNode", down:"buttonGetServer", left:"selectServerId", right:null};
	elementsTable["buttonGetServer"] = {up:"buttonSetServer", down:null, left:null, right:null};
}

function initActions() {
	$("buttonSetNode").onclick=onSetNode;
	$("buttonGetNode").onclick=onGetNode;
	$("buttonSetServer").onclick=onSetServer;
	$("buttonGetServer").onclick=onGetServer;
}

function initSelects() {
	updateSelectNodeId();
	updateSelectServerId();
}

function updateSelectNodeId() {
	var selectEl = $("selectNodeId");
	selectEl.options.length=0;
	var nodeIds = omaNodes.getNodeIds();
  	for(var item in nodeIds) {
  		var option = document.createElement("option");
  		option.text = nodeIds[item].name;
  		option.setAttribute("value", nodeIds[item].value);
  		selectEl.add(option);
  }
	selectEl.selectedIndex=0;
}

function updateSelectServerId() {
	var selectE2 = $("selectServerId");
	selectE2.options.length=0;
	var serverIds = omaNodes.getServerIds();
  	for(var item in serverIds) {
  		var option = document.createElement("option");
  		option.text = serverIds[item].name;
  		option.setAttribute("value", serverIds[item].value);
  		selectE2.add(option);
  }
	selectE2.selectedIndex=0;
}

function moveTo(target) {
	if (target) {
		$(activeElement).className="";
		activeElement = target;
		$(activeElement).className="active";
		if((target!="selectNodeId")&&(target!="selectServerId")){
			$(activeElement).focus();
		}
	}
}

function onKeyDown(e) {

	var elActive = $(activeElement);
	dump("zhouling gaia : ---onKeyDown,key = "+e.key);
	switch(getKeyCode(e.key)) {
		case 0://enter
			if (elActive.nodeName.toLowerCase() === "input")
				elActive.value = "";
			elActive.focus();
			break;
		case 1://up
			moveTo(elementsTable[elActive.id].up);
			break;
		case 2://down
			moveTo(elementsTable[elActive.id].down);
			break;
		case 3://left
			moveTo(elementsTable[elActive.id].left);
			break;
		case 4://right
			moveTo(elementsTable[elActive.id].right);
			break;
		case 5://Backspace
			dump("zhouling gaia : ---onKeyDown, Backspace");
			window.close();
			break;
		default: //key not supported
			break;
	}
	
}

function getSelectedNodeId() {
	return $("selectNodeId").selectedOptions[0].value;
}

function onGetNode() {
	var id = getSelectedNodeId();
	dump("zhouling gaia : ---onGetNode, id = "+id);

	navigator.OmaService.getDMConfigList(id,
		function(cfgValue) {
		    dump("zhouling gaia : ---onGetNode, nodeValue = "+cfgValue);
		    $("inputNodeValue").value=cfgValue;
		}
	);
}

function onSetNode() {
	var id = getSelectedNodeId();
	var nodeValue = $("inputNodeValue").value;
	dump("zhouling gaia : ---onSetNode, id = "+id+" ;nodeValue = "+nodeValue);
	navigator.OmaService.setDMNodeValue({
										"nodeIndex" : id,
										"nodeVal"   : nodeValue
										},onSetNodeCb);
}

function onGetServer() {
	navigator.OmaService.getDMConfigList(0,
		function(cfgValue) {
		    dump("zhouling gaia :  -----cfgOMADMCb="+cfgValue);
		    $("currentServerValue").value = cfgValue;
		}
	);
}

function onSetServer() {
	var nodeValue = $("selectServerId").selectedOptions[0].text;
	dump("zhouling gaia : ---onSetServer,nodeValue = "+nodeValue);
	navigator.OmaService.setDMNodeValue({
										"nodeIndex" : 0,
										"nodeVal"   : nodeValue
										},onSetNodeCb);
}

function onSetNodeCb(nodeIndex,result) {
	dump("zhouling gaia :  -----onSetNodeCb,nodeIndex = "+nodeIndex+",result = "+result);
}

window.addEventListener('load', onLoad);
window.addEventListener('keydown', onKeyDown);


