/* jshint -W098 */
define(['require','debug','view','lib/bind'],function(require) {


/**
 * Dependencies
 */

var debug = require('debug')('view:ftu-screen');
var View = require('view');
var bind = require('lib/bind');

return View.extend({
  name: 'ftu-screen',

  initialize: function() {
    this.onKeyDown = this.onKeyDown.bind(this);
  },

  render: function() {
    this.el.innerHTML = this.template();
    this.els.ftu = this.find('.js-ftu');
    setTimeout(() => {this.el.focus();}, 0);
    return this.bindEvents();
  },

  bindEvents: function() {
    bind(this.el, 'keydown', this.onKeyDown);
    return this;
  },

  onKeyDown: function(e) {
    switch (e.key) {
      case 'Enter':
        this.emit('closeftu');
        break;

      case 'Backspace':
        e.preventDefault();
        this.emit('closeftu');
        break;
    }
  },
  
  template: function() {
    return `<div class="ftu-container js-ftu" tabindex="-1">
              <div class="ftu-image"></div>
              <div class="ftu-title" data-l10n-id="welcome" ></div>
              <div class="ftu-info p-pri" data-l10n-id="ftu-info" ></div>
              <div class="ftu-softkey csk" data-l10n-id="ok"></div>
            </div>`;
  }

});

});
