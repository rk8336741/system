/**
 * The apn list panel
 */
define(['require','shared/toaster','modules/mvvm/list_view','modules/settings_panel','modules/settings_cache','modules/settings_service','modules/apn/apn_settings_manager','panels/apn_list/apn_template_factory'],function(require) {
  

  var Toaster = require('shared/toaster');
  var ListView = require('modules/mvvm/list_view');
  var SettingsPanel = require('modules/settings_panel');
  var SettingsCache = require('modules/settings_cache');
  var SettingsService = require('modules/settings_service');
  var ApnSettingsManager = require('modules/apn/apn_settings_manager');
  var ApnTemplateFactory = require('panels/apn_list/apn_template_factory');

  return function ctor_apn_settings_panel() {
    var _focusId = 0;
    itemsOBJ = {};
    var _serviceId = 0;
    var _apnType = 'default';
    var _rootElement;
    var _mainHeader;
    var _header;
    var _apnListViewRoot;
    var _apnListView;
    var _addApnBtn;
    var _role;
    var _softwareKeyRight; //right key

    var HEADER_L10N_MAP = {
      'default': 'dataSettings-header',
      'mms': 'messageSettings-header',
      'ims': 'imsSettings-header2',
      'supl': 'suplSettings-header',
      'dun': 'dunSettings-header'
    };

    var _returnTop = function() {
      SettingsService.navigate('apn-settings');
    };

    var _showToaster = function(l10nId, args) {
      Toaster.showToast({
        messageL10nId: l10nId,
        messageL10nArgs: args,
        latency: 2000,
        useTransition: true
      });
    };

    var _onApnItemEdit = function(focusItem) {
      SettingsService.navigate('apn-editor', {
        mode: 'edit',
        serviceId: _serviceId,
        type: _apnType,
        item: focusItem
      });
    };

    var _onApnItemDefaultSet = function(serviceId, apnType, radio, focusItem) {
      var setActive = function() {
        focusItem.active = true;
        radio.checked = true;
        ApnSettingsManager.setActiveApnId(serviceId, apnType, focusItem.id);
        _showToaster('changessaved');
      };

      SettingsCache.getSettings(function(results) {
        if (results['ril.data.roaming_enabled'] === true) {
          // Only display the warning when roaming is enabled.
          setActive();
        } else {
          // XXX: We need to make this to the next tick to the UI gets updated
          // because we've prevented the default behavior in the handler.
          setTimeout(function() {
            setActive();
          });
        }
      });
    };

    // after we deleted an apn , we should remove it from apn-list
    // and reset focus && navigation map && refresh softkeys
    var _onApnDelete = function bp_onApnDelete(rootElement, apnName) {
      var _self = this;
      var focusElement = rootElement.querySelector('li.focus');
      var nextElement = null;
      if (focusElement.nextSibling) {
        nextElement = focusElement.nextSibling;
      } else if (focusElement.previousSibling) {
        nextElement = focusElement.previousSibling;
      }
      focusElement.parentNode.removeChild(focusElement);
      // if apn-list is empty , refresh softkeys
      if (!nextElement) {
        SettingsSoftkey.init(_self.softkeyParamsEmpty);
        SettingsSoftkey.show();
      }

      var event = new CustomEvent('panelready', {
        detail: {
          current: '#apn-list',
        }
      });
      window.dispatchEvent(event);
      _showToaster('apn-deleted', {
        apnName: apnName
      });
    };

    var _onApnAddAction = function(serviceId, apnType, rootElement) {
      _focusId = _getFocusPos(rootElement);
      SettingsService.navigate('apn-editor', {
        mode: 'new',
        serviceId: serviceId,
        type: apnType
      });
    };

    var _onBackBtnClick = function() {
      if (_role === 'activity') {
        _role = null;
        Settings.finishActivityRequest();
      } else {
        SettingsService.navigate('apn-settings');
      }
    };

    //Defect858-tf-zhang@t2mobile.com-modify-begin
    //add by task 5224371 tf-zhang@t2mobile.com begin
    var _onItemFocus = function () {
      //find the focus element
      var theFocusItemId = _rootElement.querySelector('.focus label').dataset.id;
      var theFocusElement = itemsOBJ[theFocusItemId];
       _softwareKeyRight = SettingsSoftkey.getSoftkey().getRightKey();
      //if(theFocusElement['category'] == 'preset'){
        // _softwareKeyRight.style.visibility='hidden';
        var mind_=theFocusElement['apn']; 
        var  isOk=mind_['read_only']=='true'?true:false;
        if(isOk){
        SettingsSoftkey.disRigthKey();
        _softwareKeyRight.setAttribute('data-l10n-id', 'apn-details');//Defect3265-tf-zhang@t2mobile.com-add
      } else if(theFocusElement['category'] == 'custom'){
        // _softwareKeyRight.style.visibility='visible';
        SettingsSoftkey.enaRightKey();
        _softwareKeyRight.setAttribute('data-l10n-id', 'options');//Defect3265-tf-zhang@t2mobile.com-add
      }
    };

    var _onItemBlur = function () {
      // _softwareKeyRight.style.visibility='visible';
      // SettingsSoftkey.enaRightKey();
    };
    //add by task 5224371 tf-zhang@t2mobile.com end
    //Defect858-tf-zhang@t2mobile.com-modify-end

    function _updateUI() {
      ApnSettingsManager.queryApns(_serviceId, _apnType).then(apnItems => {
        Object.keys(apnItems).forEach(key => {
          if (apnItems[key].active) {
            var activeApnItem =
              _rootElement.querySelector('[data-id="' + apnItems[key].id + '"]');
            activeApnItem && (activeApnItem.querySelector('input').checked = true);
          }
        });
      });
    }

    return SettingsPanel({
      onInit: function bp_onInit(rootElement) {
        var _self = this;
        this.softkeyParams = {
          header: {l10nId: 'fxa-options'},
          items: [{
            name: 'Add Apn',
            l10nId: 'add-apn',
            priority: 1,
            method: function() {
              _onApnAddAction(_serviceId, _apnType, rootElement);
            }
          }, {
            name: 'Select',
            l10nId: 'select',
            priority: 2,
            method: function() {
              if (!rootElement.querySelector('.focus label')) {
                return;
              }
              var dataItemId = rootElement.querySelector('.focus label').dataset.id;
              var radioElement = rootElement.querySelector('.focus input');
              _onApnItemDefaultSet(_serviceId, _apnType, radioElement, itemsOBJ[dataItemId]);
              SettingsSoftkey.hide();
              _returnTop();
            }
          }, {
            name: '',
            l10nId: '',
            priority: 3,
            method: function() {
              _focusId = _getFocusPos(rootElement);
              var dataItemId = rootElement.querySelector('.focus label').dataset.id;
              _onApnItemEdit(itemsOBJ[dataItemId]);
            }
          }, {
            name: 'Edit',
            l10nId: 'edit',
            priority: 7,
            method: function() {
              _focusId = _getFocusPos(rootElement);
              var dataItemId = rootElement.querySelector('.focus label').dataset.id;
              _onApnItemEdit(itemsOBJ[dataItemId]);
            }
          }, {
            name: 'Delete',
            l10nId: 'delete-apn',
            priority: 8,
            method: function() {
              var dataItemId = rootElement.querySelector('.focus label').dataset.id;
              var focusItem = itemsOBJ[dataItemId];
              var dialogConfig = {
                title: {id: 'confirmation', args: {}},
                body: {
                  id: 'delete-apn-confirm',
                  args: {
                    apnName: focusItem.apn.apn
                  }
                },
                cancel: {
                  l10nId: 'cancel',
                  priority: 1,
                  callback: function() {},
                },
                confirm: {
                  l10nId: 'delete-apn',
                  priority: 3,
                  callback: function() {
                    if (focusItem.apn.deletedCpApn === false) {
                      focusItem.apn.deletedCpApn = true;
                      ApnSettingsManager.updateApn(_serviceId, focusItem.id,
                        focusItem.apn).then(setTimeout(() => {
                          _onApnDelete(_rootElement, focusItem.apn.apn);
                        }, 100)).then(() => {
                          _updateUI();
                        }
                      );
                    } else {
                      ApnSettingsManager.removeApn(_serviceId, focusItem.id).then(
                        setTimeout(() => {
                          _onApnDelete(_rootElement, focusItem.apn.apn);
                        }, 100)).then(() => {
                          _updateUI();
                        }
                      );
                    }
                  },
                },
              };
              var dialog = new ConfirmDialogHelper(dialogConfig);
              dialog.show(document.getElementById('app-confirmation-dialog'));
            }
          }, ],
        };

        this.softkeyParamsEmpty = {
          menuClassName: 'menu-button',
          header: {l10nId: 'message'},
          items: [{
            name: 'Add Apn',
            l10nId: 'add-apn',
            priority: 1,
            method: function() {
              _onApnAddAction(_serviceId, _apnType);
            }
          }, ]
        };
        _rootElement = rootElement;
        _mainHeader = rootElement.querySelector('gaia-header');
        _header = _mainHeader.querySelector('h1');
        _apnListViewRoot = rootElement.querySelector('.apn-list');
        _addApnBtn = rootElement.querySelector('button.add-apn');
        _mainHeader.addEventListener('action', _onBackBtnClick);
      },
      onBeforeShow: function bp_onBeforeShow(rootElement, options) {
        var _self = this;
        _role = options.role || _role;

        SettingsSoftkey.init(_self.softkeyParamsEmpty);
        SettingsSoftkey.show();
        if (options.type) {
          _focusId = 0;
        }
        // When back from apn editor, there is no type and serviceId specified
        // so that we use the original type and service id.
        _apnType = options.type || _apnType;
        _serviceId = (options.serviceId === undefined) ?
          _serviceId : options.serviceId;

        _header.setAttribute('data-l10n-id', HEADER_L10N_MAP[_apnType]);

        var apnTemplate =
          ApnTemplateFactory(_apnType, null, null);

        ApnSettingsManager.queryApns(_serviceId, _apnType)
          .then(apnItems => {
            //add by task5361532 tf-zhang@t2mobile.com begin
            // One line modification by lina.zhang@t2mobile.com for bug4642 20180612
            var req = window.navigator.mozSettings.createLock().get('apn_default_show_' + _serviceId);
            var config = null;
            req.onsuccess = function () {
              // One line modification by lina.zhang@t2mobile.com for bug4642 20180612
              config = req.result['apn_default_show_' + _serviceId];
              var prepostpayApns = ['wap.vodafone.co.uk', 'pp.vodafone.co.uk'];
              var shouldHide = '', shouldShow = '';
              if (config === prepostpayApns[0] && config !== prepostpayApns[1]) {
                shouldShow = prepostpayApns[0];
                shouldHide = prepostpayApns[1];
              } else if (config === prepostpayApns[1] && config !== prepostpayApns[0]) {
                shouldHide = prepostpayApns[0];
                shouldShow = prepostpayApns[1];
              }
              console.log('prepostpayapn config = ' + config + ', shouldHide = ' + shouldHide);
              var apnItems1 = [], j = 0, shouldSetActive = true;
              for (var i in apnItems) {
                console.log('prepostpayapn i = ' + i + ', apnItems  = ' + JSON.stringify(apnItems[i]));
                if (apnItems[i]['_apn']['apn'] !== shouldHide) {
                  apnItems1[j] = apnItems[i];
                  if (apnItems1[j].active) {
                    shouldSetActive = false;
                  }
                  j++;
                }
              }
              console.log('prepostpayapn shouldSetActive = ' + shouldSetActive);
              if (shouldSetActive) {
                for (j = 0; j < apnItems1.length; j++) {
                  if (shouldShow === apnItems1[j]['_apn']['apn']) {
                    apnItems1[j].active = true;
                    break;
                  }
                }
                if (j == apnItems1.length) {
                  apnItems1[0].active = true;
                }
              }

              SettingsSoftkey.init(apnItems1.length ? _self.softkeyParams : _self.softkeyParamsEmpty);
              SettingsSoftkey.show();

              console.log('prepostpayapn apnItems1 = ' + JSON.stringify(apnItems1));
              _apnListView = ListView(_apnListViewRoot, apnItems1, apnTemplate);
              var needFocused = options.action === 'edit' ? _getElementByFocusId(_focusId, rootElement) : _getElementByFocusId(_focusId, rootElement, true);
              var event = new CustomEvent('panelready', {
                detail: {
                  current: '#' + rootElement.id,
                  previous: options.type ? '#apn-settings' : '#apn-editor',
                  needFocused: needFocused
                }
              });
              setTimeout(function () {
                window.dispatchEvent(event);
              }, 350);

              //add by task 5224371 tf-zhang@t2mobile.com begin
              var cnodes = _apnListView['element'].childNodes;
              for (var i = 0; i < cnodes.length; i++) {
                cnodes[i].addEventListener('focus', _onItemFocus);
                cnodes[i].addEventListener('blur', _onItemBlur);
              }
              //add by task 5224371 tf-zhang@t2mobile.com end
            };
          });
      },

      onBeforeHide: function abp_onBeforeHide() {
        SettingsSoftkey.hide();
      },

      onHide: function bp_onBeforeHide() {
        if (_apnListView) {
          _apnListView.destroy();
          _apnListView = null;
        }
      }
    });
  };
});
