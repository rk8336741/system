define([ "require", "cards", "l10n!", "./base", "template!./setup_account_prefs.html", "./editor_mixins", "./account_prefs_mixins" ], function(e) {
    var t, n = e("cards"), o = e("l10n!"), r = /\s+$/;
    return [ e("./base")(e("template!./setup_account_prefs.html")), e("./editor_mixins"), e("./account_prefs_mixins"), {
        onArgs: function(e) {
            this.account = e.account, this.identity = this.account.identities[0], this.signatureNodePref.innerHTML = "<br>";
            var n = o.get("settings-default-signature-2");
            this.identity.modifyIdentity({
                signatureEnabled: !0,
                signature: n
            });
            var r = navigator.mozSettings.createLock().get("Customization.email.Signature");
            t = this, r.onsuccess = function() {
                var e = r.result["Customization.email.Signature"];
                dump("query Customization.email.Signature success  customizationSinature = " + e), 
                e && "" !== e && (n = e, t.identity.modifyIdentity({
                    signature: n
                })), t._bindPrefs("tng-account-check-interval", "tng-notify-mail", "tng-sound-onsend", "tng-signature-enable", "signature-box", "pref-account-label"), 
                t._bindEditor(t.signatureNodePref);
            }, r.onerror = function() {
                dump("query Customization.email.Signature failed!"), t._bindPrefs("tng-account-check-interval", "tng-notify-mail", "tng-sound-onsend", "tng-signature-enable", "signature-box", "pref-account-label"), 
                t._bindEditor(t.signatureNodePref);
            };
        },
        getTextFromEditor: function() {
            var e = this.fromEditor().replace(r, "");
            return e;
        },
        onNext: function() {
            var e = t.getTextFromEditor(), o = t.prefAccountLabelNode.value.trim();
            e !== t.identity.signature && t.identity.modifyIdentity({
                signature: e
            }), o !== t.account.label && t.account.modifyAccount({
                label: o
            }), n.pushCard("setup_done", "animate");
        },
        updateFocusList: function() {
            NavigationMap.navSetup("cards-setup-account-prefs", "cards-setup-account-prefs ul li:not(.hidden)");
        },
        handleKeyDown: function(e) {
            switch (e.key) {
              case "Accept":
              case "Enter":
                var t = document.querySelector(".scrollregion-below-header .focus select");
                t && t.focus();
            }
            window.option.buttonCsk.textContent = document.activeElement.classList.contains("no-select") ? document.activeElement.classList.contains("signature-box") ? navigator.mozL10n.get("enter") : "" : navigator.mozL10n.get("select");
        },
        onBodyNodeKeydown: function(e) {
            function t(e) {
                return "BR" === document.activeElement.lastChild.tagName && e.startOffset === document.activeElement.childNodes.length - 1 ? !0 : e.startOffset === document.activeElement.childNodes.length ? !0 : !1;
            }
            function n(e) {
                return e.startOffset === e.startContainer.length ? !0 : !1;
            }
            var o = window.getSelection().getRangeAt(0), r = o.startContainer;
            switch (e.key) {
              case "ArrowUp":
                if (!(r !== document.activeElement && r !== document.activeElement.firstChild || 0 !== o.startOffset && 0 !== r.textContent.length)) break;
                e.stopPropagation();
                break;

              case "ArrowDown":
                if (r === document.activeElement && t(o) || n(o)) break;
                e.stopPropagation();
            }
        },
        onCardVisible: function(e) {
            const n = this.localName, o = n + " " + "ul li:not(.hidden)", r = n + " " + o;
            var i = [ {
                name: "Select",
                l10nId: "select",
                priority: 2
            }, {
                name: "Next",
                l10nId: "setup-info-next",
                priority: 3,
                method: function() {
                    t.onNext();
                }
            } ];
            this.prefAccountLabelNode.parentNode.addEventListener("focus", function() {
                t.prefAccountLabelNode.focus(), t.prefAccountLabelNode.setSelectionRange(9999, 9999);
            }), console.log(this.localName + ".onCardVisible, navDirection=" + e), "forward" === e ? (NavigationMap.navSetup(n, o), 
            NavigationMap.setCurrentControl(r), NavigationMap.setFocus("first"), NavigationMap.setSoftKeyBar(i)) : "back" === e && (NavigationMap.setCurrentControl(r), 
            NavigationMap.setFocus("restore"), NavigationMap.setSoftKeyBar(i)), window.addEventListener("keydown", t.handleKeyDown), 
            document.addEventListener("update-list", t.updateFocusList), this.updateSignatureBox();
            var s = document.querySelector("cards-setup-account-prefs .signature-box"), a = document.querySelector("cards-setup-account-prefs .settings-account-signature");
            a.addEventListener("focus", function(e) {
                if (s.focus(), !e.relatedTarget.classList.contains("signature-box")) {
                    var t = window.getSelection(), n = document.createRange(), o = s.lastElementChild;
                    "BR" === o.nodeName ? n.setStartBefore(o) : n.setStartAfter(o), t.removeAllRanges(), 
                    t.addRange(n);
                }
            }), document.activeElement.classList.contains("no-select") && (window.option.buttonCsk.textContent = "");
        },
        die: function() {
            window.removeEventListener("keydown", t.handleKeyDown), document.removeEventListener("update-list", t.updateFocusList);
        }
    } ];
});