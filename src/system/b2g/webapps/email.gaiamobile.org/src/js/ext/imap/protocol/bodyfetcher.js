define([], function() {
    function e(e, n, t) {
        this.connection = e, this.parserClass = n, this.list = t, this.pending = t.length, 
        this.onparsed = null, this.onend = null, t.forEach(this._fetch, this);
    }
    return e.prototype = {
        _fetch: function(e) {
            this.connection.listMessages(e.uid, [ "BODY.PEEK[" + (e.partInfo.partID || "1") + "]" + (e.bytes ? "<" + e.bytes[0] + "." + e.bytes[1] + ">" : "") ], {
                byUid: !0
            }, function(n, t) {
                if (n) this._resolve(n, e, null); else {
                    var r = new this.parserClass(e.partInfo), o = t[0], s = null;
                    for (var a in o) if (/^body/.test(a)) {
                        s = o[a];
                        break;
                    }
                    s ? (r.parse(s), this._resolve(null, e, r.complete())) : this.resolve("no body", e);
                }
            }.bind(this));
        },
        _resolve: function(e, n, t) {
            this.onparsed && this.onparsed(e, n, t), !--this.pending && this.onend && this.onend();
        }
    }, {
        BodyFetcher: e
    };
});