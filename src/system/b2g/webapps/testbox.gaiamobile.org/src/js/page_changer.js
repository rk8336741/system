/**
 * Created by zhuchunlong on 4/1/16.
 */

//存储当前的section的堆栈，当点击a标签跳转到其他section时，该section入栈；当回退到前一个section时，当前section出栈
var curSectionStack;

//当前点击的跳转按钮的栈，为了在返回到前一个section时设置focus用
var curLinkStack;

//当前页面中所有的section id,可以存section元素，但是可能消耗内存多点...
var sectionIds;

var pageChanger = {
    init : function engmodeEx_init(){

        curSectionStack = new Array();
        curLinkStack = new Array();

        getAllSectionIds();

        curSectionStack.push(sectionIds[0]);

        var pageLinks = getASection();
        for(var i = 0, link; link = pageLinks[i]; i++){
            link.addEventListener("click" , function(event){
                var url = this.href;
                var sectionId = url.substr(url.indexOf('#') + 1);

                //之前代码中跳转section时会有这个逻辑，若代码通用时不需要该方法
                testBoxSection(sectionId);

                //查找该sectionId在sectionIds中的位置
                var index = sectionIds.indexOf(sectionId);

                if(index != -1){
                    if(sectionId != curSectionStack[curSectionStack.length - 2]){//向前跳转到前一个section
                        //获取当前想要跳转到的section
                        var curSection = document.getElementById(sectionIds[index]);
                        gotoSection(curSection);//跳转到下一个section
                        curLinkStack.push(this);//将当前点击跳转的link记录下来，以便返回时选择焦点

                        var focusItems =  curSection.querySelectorAll('[data-type="focusEnabled"]');
                        //设置显示的section的焦点为其第一个焦点元素
                        setFocus(focusItems[0]);
                    } else {//返回到上一个section
                        pageBack();
                    }
                }

            });
        }
    }
}

function setFocus(item){
    window.setTimeout(function() {
        item.focus();
    }, 200);
}

function pageBack(){
    gotoBack();
    //设置显示的section的焦点为其点击跳转时记录下来的点击元素
    setFocus(curLinkStack.pop());
}

/**
 * 存储所有的section id
 */
function getAllSectionIds(){
    var allSection = document.getElementsByTagName('section');
    sectionIds = new Array();
    for(var i = 0, sec; sec = allSection[i]; i++){
        sectionIds[i] = sec.id;
    }
}

/**
 * 获取带section跳转的a元素，根据需求自定义
 */
function getASection(){
    return document.getElementsByTagName('a');
}


function gotoBack(){
    if(curSectionStack.length == 0){//栈中没有section时，不处理gotoBack事件
        return;
    }
    curSectionStack.pop();
    var section = document.getElementById(curSectionStack[curSectionStack.length - 1]);
    view(section);
}

function gotoSection(section){
    curSectionStack.push(section.id);
    view(section);
}

function view(section){
    for(var i = 0, sec; sec = sectionIds[i]; i++){
        if(section.id == sec){
            section.style.display = 'block';
            continue;
        }
        document.getElementById(sec).style.display = 'none';
    }
}

/**
 * 之前代码中跳转section时会有这个逻辑，若代码通用时不需要该方法
 * @param sectionId
 */
function testBoxSection(sectionId){
    switch (sectionId) {
        case 'wifiTx':
        case 'wifiRx':
            if (navigator.engmodeExtension) {
                var engmodeEx = navigator.engmodeExtension;
                var parmArray = new Array();
                parmArray.push('wifitest');
                parmArray.push('power');
                parmArray.push('/data/testbox_log/wifitext.txt');
                var initRequest = engmodeEx.execCmdLE(parmArray, 3);
            }
            break;
        case 'bluetoothTx':
            var initCommand = '/system/bin/btTx_start' +
                ' > /data/testbox_log/btTx_start.txt';
            if (navigator.engmodeExtension) {
                var engmodeEx = navigator.engmodeExtension;
                debug('lx: bluetoothTx  initCommand ' + initCommand + '\n');
                var parmArray = new Array();
                parmArray.push('btTx_start');
                parmArray.push('');
                parmArray.push('/data/testbox_log/btTx_start.txt');
                var initRequest = engmodeEx.execCmdLE(parmArray, 3);
            }
            break;

        case 'nfc':
            if (navigator.engmodeExtension) {
                var engmodeEx = navigator.engmodeExtension;
                var parmArray = new Array();
                parmArray.push('nfc_stop');
                parmArray.push('');
                parmArray.push('/data/testbox_log/nfc_stop.txt');
                var initRequest = engmodeEx.execCmdLE(parmArray, 3);
            }
            break;

        default :
            break;
    }
}


pageChanger.init();
