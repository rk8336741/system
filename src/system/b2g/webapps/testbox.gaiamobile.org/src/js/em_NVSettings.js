/**
 * Created by xa on 8/5/14.
 */
'use strict';
$('menuItem-nvsettings').addEventListener('click', function() {
  NVSettings.init();
});

var nvResultArr;
var nvLength;
var isRead = false;
var engmodeEx = navigator.engmodeExtension;
var nvItemInput = $('nv-item-input');
var nvInputBegin = $('nv-input-begin');
var nvInputEnd = $('nv-input-end');
var nvValueInput = $('nv-vaule-input');
var nvResult = $('nv-result');

var NVSettings = {
  init: function init(){
    $('nv-read').addEventListener('click', function() {
      NVSettings.getNVValue();
    });
    $('nv-write').addEventListener('click', function() {
      NVSettings.putNVValue();
    });
  },
  nvResultArrShow: function(nvResultArr) {
    var newNvResultArr = 'NV : <br>';
    var lineNum = Math.ceil(nvResultArr.length / 45);
    for(var i = 0; i < lineNum; i++) {
      newNvResultArr += (nvResultArr.substr(45 * i, 45) + '<br>');
    }
    return newNvResultArr;
  },
  getNVValue: function putNVValue(){
    var nvArray = new Array();
    var inputitemvalue = parseInt(nvItemInput.value);
    var req = engmodeEx.readNvitemEx(inputitemvalue);
    req.onsuccess = function(e) {
      debug('onsuccess: ' + req.result.join());
      nvArray = nvArray.concat(req.result);
      nvResultArr = NVSettings.hexArrayToStr(nvArray);
      nvLength = nvArray.length;
      nvInputBegin.value = '';
      nvInputEnd.value = '';
      nvValueInput.value = '';
      nvResult.innerHTML = 'Nv length = ' + nvLength + '<br>' +
                           NVSettings.nvResultArrShow(nvResultArr);
      isRead = true;
    }
    req.onerror = function(e) {
      debug('onerror: ');
      nvInputBegin.value = '';
      nvInputEnd.value = '';
      nvValueInput.value = '';
      nvResult.innerHTML = 'Can\'t read this item! \n' +
                           ' Please Check if Initialization';
    }
  },
  makeValuetoTrue: function makeValuetoTrue(){
    var nvbeginitem = parseInt(nvInputBegin.value,16);
    var nvenditem = parseInt(nvInputEnd.value,16);
    var nvValueInputs = nvValueInput.value;

    if (('' == nvInputBegin.value) || ('' == nvInputEnd.value)
      || (((nvenditem * 5) - 1) != nvValueInputs.length)
      || (nvenditem > nvLength)) {
      return 'err';
    } else {
      if (nvbeginitem > 0) {
        if(nvbeginitem == (nvLength - nvenditem)) {
          return nvResultArr.substring(0, nvbeginitem * 5)
            + nvValueInputs;
        } else {
          return nvResultArr.substring(0, nvbeginitem * 5)
            + nvValueInputs + ','
            + nvResultArr.substring(((nvbeginitem + nvenditem) * 5), (nvLength * 5) - 1);
        }

      } else if (0 == nvbeginitem) {
        if(nvenditem == nvLength) {
          return nvValueInputs;
        } else {
          return nvValueInputs + ','
            + nvResultArr.substring((nvenditem * 5), (nvLength * 5) - 1);
        }
      }
    }
  },
  putNVValue: function putNVValue(){
    var nvsetArray = new Array();
    var nvsetItem = nvItemInput.value;
    var nvsetLength = nvLength;
    var nvsetVlaue = NVSettings.makeValuetoTrue();
    var nvsetVlaueLength;

    if(true != isRead) {
      alert('Please read the NV first');
      return null;
    } else {
      if ('err' == nvsetVlaue) {
        alert('Please check the NV you input');
        return null;
      }
    }

    nvsetVlaue = nvsetVlaue.replace(/,/g,'');
    nvsetVlaue = nvsetVlaue.replace(/0X/g,'');
    nvsetVlaue = nvsetVlaue.replace(/0x/g,'');
    nvsetVlaue = nvsetVlaue.replace(/ /g,'');
    if(0 == (nvsetVlaue.length)%2) {
      nvsetVlaueLength = (nvsetVlaue.length) / 2;
    } else {
      nvsetVlaueLength = (nvsetVlaue.length + 1) / 2;
    }
    if(nvsetVlaueLength)
    for(var i = 0; i < nvsetVlaueLength; i++){
      var num = nvsetVlaue.slice(i*2, i*2 + 2);
      nvsetArray[i] = parseInt(num,16);
    }

    var req = engmodeEx.writeNvitemEx(nvsetItem, nvsetLength, nvsetArray);
    req.onsuccess = function(e) {
      debug('writeNvitemEx onsuccess: ');
      isRead = false;
      nvsetItem.value = '';
      nvValueInput.value = '';
      nvResult.innerHTML = '';
      alert('NV Set Seuccessed!');
      NVSettings.getNVValue();
    }
    req.onerror = function(e) {
      debug('writeNvitemEx onerror: ');
      nvResult.innerHTML = '';
      alert('NV Set Failed!');
    }
  },
  hexArrayToAsciiStr: function(arr) {
    debug('traceData.hexArrayToAsciiStr');
    var ascii = [];
    for (var i in arr) {
      //ASCII character scope
      if (arr[i] >= 32 && arr[i] <= 126)
      {
        var ss = String.fromCharCode(arr[i]);
        ascii.push(ss);
      }
      else
      //ascii.push('*');
        ascii.push(' ');
    }
    var ascii_code = ascii.join('');
    debug(' ascii_code = ' + ascii_code);
    return ascii_code;
  },

  hexArrayToStr: function(arrayItem) {
    debug('traceData.hexArrayToStr');
    var hexStr = '';
    for (var i in arrayItem) {
      var ss = arrayItem[i].toString(16);
      if(2 != ss.length) {
        ss = '0' + ss;
      }
      if(0 == i) {
        ss = '0X' + ss;
      } else {
        ss = ',0X' + ss;
      }
      hexStr += ss;
    }
    hexStr = hexStr.toUpperCase();
    debug(' hexStr = ' + hexStr);
    return hexStr;
  }
}
