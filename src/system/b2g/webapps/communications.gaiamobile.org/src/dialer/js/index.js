/* globals KeypadManager, NavbarManager, LazyLoader, CallHandler */
'use strict';

function onSecondLazyLoad() {
  var lazyFiles = [
    '/shared/style/animations.css',
    'style/toolbar.css',
    '/shared/style/action_menu.css',
    '/shared/style/toaster.css',
    '/shared/style/contacts/contact_in_overlay.css',
    '/shared/style/dialer/overlay.css',
    '/dialer/js/cache.js',

    '/shared/js/performance_testing_helper.js',

    '/shared/js/font_size_utils.js',
    '/dialer/js/utils.js',
    '/dialer/js/dialer.js',
    '/dialer/js/call_log_db.js',
    'js/call_log_menu.js',
    '/shared/js/toaster.js',
    '/shared/style/toaster.css',
    '/shared/js/component_utils.js',
    '/shared/elements/gaia_confirm/script.js',
    '/shared/elements/gaia_tabs/script.js',
    '/dialer/js/pbmap/contact2vcard.js',
    '/dialer/js/pbmap/map_manager.js',
    '/dialer/js/pbmap/message.js',
    '/dialer/js/pbmap/message_utils.js',
    '/dialer/js/pbmap/pbap.js',
    '/dialer/js/pbmap/phonebook.js',
    '/dialer/js/pbmap/pbmap_iac_api.js',
    '/dialer/js/pbmap/setImmediate.js',
    '/dialer/js/pbmap/vcard_utils.js',
    '/shared/js/homescreens/confirm_dialog_helper.js',
    '/shared/js/dialer/tone_player.js',
    '/shared/js/dialer/dtmf_tone.js',
    '/shared/js/dialer/font_size_manager.js',
    '/shared/js/second_screen.js',
    '/dialer/js/photo_resize.js',
    '/shared/js/image_utils.js'
  ];
  LazyLoader.load(lazyFiles, function () {
    onLoadDialer();
  });
}

function onFirstLazyLoad() {
  setTimeout(() => {
    var lazyFiles = [
      '/shared/js/settings_listener.js',
      '/shared/js/l10n.js',
      '/shared/js/l10n_date.js',
      '/shared/js/navigation_handler.js',
      '/shared/js/navigation_manager.js',
      '/shared/js/mobile_operator.js',
      '/shared/js/option_menu.js',
      '/shared/style/option_menu.css',
      '/shared/js/option_menu_helper.js',
      '/shared/js/softkey_panel.js',
      '/shared/js/softkey_panel_helper.js',
      'js/selected_item_menu_params.js',
      'js/group_menu_params.js'
    ];
    window.performance.mark('visuallyLoaded');
    LazyLoader.load(lazyFiles, function () {
      navigator.mozL10n.once(() => {
        var _ = navigator.mozL10n.get;
        var header = document.querySelector('#calllog');
        header.textContent = _(header.getAttribute('data-l10n-id'));
        var callLogContainer = document.getElementById('call-log-container');
        var item = callLogContainer.querySelector('section li');
        if (!item) {
        } else if (item.dataset.contactId) {
          OptionHelper.show('log-item');
        } else {
          OptionHelper.show('log-item-new');
        }
      });
      onSecondLazyLoad();
    });
  }, pendingTime);;
}


function onLoadDialer() {
  window.dispatchEvent(new CustomEvent('moz-chrome-dom-loaded'));
  window.dispatchEvent(new CustomEvent('moz-app-visually-complete'));

  window.removeEventListener('load', onFirstLazyLoad);

  /* XXX: Don't specify a default volume control channel as we want to stick
   * with the default one as a workaround for bug 1092346. Once that bug is
   * fixed please add back the following line:
   *
   * navigator.mozAudioChannelManager.volumeControlChannel = 'notification';
   */

  // Keypad (app core content) is now bound
  window.performance.mark('contentInteractive');
  window.dispatchEvent(new CustomEvent('moz-content-interactive'));

  NavbarManager.init();
  // Navbar (chrome) events have now been bound
  window.performance.mark('navigationInteractive');
  window.dispatchEvent(new CustomEvent('moz-chrome-interactive'));

  var lazyPanels = ['confirmation-message',
                    'edit-mode',
                    'sim-picker'];

  var lazyPanelsElements = lazyPanels.map(function toElement(id) {
    return document.getElementById(id);
  });
  LazyLoader.load(lazyPanelsElements);

  setTimeout(function nextTick() {
    CallHandler.init();
    navigator.mozL10n.once(function loadLazyFilesSet() {
      if (CallLogCacheRestore.restored) {
        var codeNode = document.getElementById('call-log-view');
        var dataL10ns = codeNode.querySelectorAll('[data-l10n-id]');
        var _ = navigator.mozL10n.get;
        for (var i = 0; i < dataL10ns.length; i++) {
          dataL10ns[i].textContent = _(dataL10ns[i].getAttribute('data-l10n-id'));
        }
      }
      LazyLoader.load([
        '/shared/js/fb/fb_request.js',
        '/shared/js/fb/fb_data_reader.js',
        '/shared/js/fb/fb_reader_utils.js',
        '/shared/style/confirm.css',
        '/shared/js/confirm.js',
        '/shared/elements/config.js',
        '/shared/elements/gaia-header/dist/gaia-header.js',
        '/shared/style/edit_mode.css'
      ], function fileSetLoaded() {
        window.performance.mark('fullyLoaded');
        window.dispatchEvent(new CustomEvent('moz-app-loaded'));
      });
    });

  });
}

var pendingTime = 0;
if (CallLogCacheRestore.hydrateHtml('cache-list')) {
  document.getElementById('call-log-filter').classList.remove('hide');
  var el = document.querySelector('#call-log-filter #all-filter');
  el.setAttribute('aria-selected', 'true');
  el.classList.add('selected');
  window.performance.mark('navigationLoaded');
  pendingTime = 460;
} else {
  document.querySelector('#calllog').textContent = '';
}

document.body.classList.toggle('large-text', navigator.largeTextEnabled);

window.addEventListener('load', onFirstLazyLoad);
var handlerBeforeFullLoad = function _handlerBeforeFullLoad (evt) {
  switch(evt.key) {
    case 'ArrowDown':
    case 'ArrowUp':
      evt.preventDefault();
      evt.stopPropagation();
      break;
    case 'Call':
    case 'Enter':
      var noResultContainer = document.getElementById('no-result-container');
      if (!noResultContainer.hidden) {
        return;
      }
      navigator.mozL10n.once(() => {
        navigator.hasFeature('device.capability.vilte').then((hasVT) => {
          CallMenu.dial(CallLogCacheRestore.defaultServiceId, null, !hasVT);
        });
      });
      break;
  }
};
window.addEventListener('keydown', handlerBeforeFullLoad, true);
window.addEventListener('first-call-logs-ready', function removePreHandler() {
  window.removeEventListener('first-call-logs-ready', removePreHandler);
  window.removeEventListener('keydown', handlerBeforeFullLoad, true);
});

