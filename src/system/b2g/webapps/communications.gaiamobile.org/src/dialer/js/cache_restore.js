/* (c) 2017 KAI OS TECHNOLOGIES (HONG KONG) LIMITED All rights reserved. This
 * file or any portion thereof may not be reproduced or used in any manner
 * whatsoever without the express written permission of KAI OS TECHNOLOGIES
 * (HONG KONG) LIMITED. KaiOS is the trademark of KAI OS TECHNOLOGIES (HONG KONG)
 * LIMITED or its affiliate company and may be registered in some jurisdictions.
 * All other trademarks are the property of their respective owners.
 */

(function(exports) {
  'use strict';

  window.HTML_CACHE_VERSION = '1.02-2';

  var calllogcacherestore = {
    restored: false,
    defaultServiceId: -1,
    hydrateHtml: function cache_hydrateHtml(id) {
      var parsedResults = this.retrieve(id);
      if (parsedResults.lang !== navigator.language) {
        window.dispatchEvent(new CustomEvent('cache-restored'));
        return false;
      }

      var contents = parsedResults.contents;
      if (contents === '') {
        window.dispatchEvent(new CustomEvent('cache-restored'));
        return false;
      }
      var head = document.getElementById('calllog');
      head && (head.textContent = parsedResults.headContent);
      var cardsNode = document.getElementById('call-log-container');
      var softkey = document.getElementById('fakeSoftkeyPanel');
      cardsNode.innerHTML = contents;
      this.restored = true;
      if (cardsNode.querySelector('.focus')) {
        cardsNode.querySelector('.focus').focus();
      }

      var tabFilters = {
        'allFilter': '#all-filter > span',
        'missFilter': '#missed-filter > span',
        'dialFilter': '#dialed-filter > span',
        'receiveFilter': '#received-filter > span'
      };
      var span;
      for (var i in tabFilters) {
        span = document.querySelector(tabFilters[i]);
        span.textContent = parsedResults[i];
      }
      if (cardsNode.querySelector('.log-item')) {
        var noResultContainer = document.getElementById('no-result-container');
        noResultContainer.hidden = true; 
        var icon = null;
        this.defaultServiceId = parsedResults.defaultServiceId
        if (parsedResults.defaultServiceId >= 0 &&
            navigator.mozMobileConnections.length > 1) {
          icon = `sim-${parsedResults.defaultServiceId + 1}`;
          softkey.querySelector('#software-keys-center').innerHTML =
            '<span data-name="Call" data-icon="' + icon + '"/>';
        } else {
          softkey.querySelector('#software-keys-center').innerHTML =
            '<span data-name="Call"/>';
        }
        softkey.querySelector('#software-keys-right').textContent =
          parsedResults.rskContent;
        softkey.querySelector('#software-keys-center > span').textContent =
          parsedResults.cskContent;
      }
      softkey.classList.add('visible');
      window.dispatchEvent(new CustomEvent('cache-restored'));
      return true;
    },

    retrieve: function cache_retrieve(id) {
      var value = localStorage.getItem('html_cache_' + id) || '';

      var index, version, lang, defaultServiceId, cskContent, rskContent;
      var headContent;
      var allFilter, missFilter, dialFilter, receiveFilter;

      index = value.indexOf(':');

      if (index === -1) {
        value = '';
      } else {
        version = value.substring(0, index);
        value = value.substring(index + 1);

        var versionParts = version.split(',');
        version = versionParts[0];
        lang = versionParts[1];
        defaultServiceId = parseInt(versionParts[2]);
        cskContent = versionParts[3];
        rskContent = versionParts[4];
        headContent = versionParts[5];
        allFilter = versionParts[6];
        missFilter = versionParts[7];
        dialFilter = versionParts[8];
        receiveFilter = versionParts[9];
      }

      if (version !== window.HTML_CACHE_VERSION) {
        value = '';
      }

      return {
        lang: lang,
        defaultServiceId: defaultServiceId,
        cskContent: cskContent,
        rskContent: rskContent,
        headContent: headContent,
        allFilter: allFilter,
        missFilter: missFilter,
        dialFilter: dialFilter,
        receiveFilter: receiveFilter,
        contents: value
      };
    }
  };

    exports.CallLogCacheRestore = calllogcacherestore;
})(this);
