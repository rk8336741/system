(function (exports) {
'use strict';
  var currentAppPromise = null;

  var PbmapIAC = {

    USER_CONFIRMATION: 1,
    OBEX_PASSWORD: 2,

    connectApp: function () {
      if (!currentAppPromise) {
        currentAppPromise = navigator.mozApps.getSelf();
      }
      return currentAppPromise;
    },

    callSystemDialog: function (options) {
      return this.connectApp().then(app => {
        return app.connect('pbmap-dialog');
      }).then(ports => {
        ports[0].postMessage(options);
        return new Promise(resolve => {
          ports[0].onmessage = function(event) {
            resolve(event.data);
          };
        });
      });
    }

  };
  exports.PbmapIAC = PbmapIAC;
})(window);
