'use strict';
/*
 * Phone Book Access Profile (PBAP) is a profile that enables devcices to
 * exchange phonebook objects via Bluetooth. After a Bluetooth connection is
 * established, Phone-book objects each represent information about a contact
 * stored on a mobile phone. This PBAP server module will handle all PBAP
 * request from the client to response the phonebook records or list, and all
 * three kinds of request from the client are:
 * - pullphonebook - pull all phonebook records based on the constraint of the
 *   filter object.
 * - pullvcardentry - pull a specific contacts.
 * - pullvcardlisting - pull a XML format list of all contacts which filtered by
 *   a filter argument.
 */

/* global PbmapIAC,ContactToVcardBlob, LazyLoader, PbapPhonebook */

(function(exports) {
  var bluetoothManager = navigator.mozBluetooth;
  var adapter = null;
  var pb;
  var pairedName;

  function isBtV2() {
    if (typeof(bluetoothManager.onattributechanged) !== 'undefined') {
      return true;
    } else {
      return false;
    }
  }

  function init() {
    if (isBtV2()) {
      watchMozBluetoothAttributechanged();
      initDefaultAdapter();
    } else {
      fetchDefaultAdapter();

      // 'adapteradded' can only be triggered by enabling Bluetooth.
      // update adapter when the Bluetooth enabled again.
      bluetoothManager.onadapteradded = function() {
        fetchDefaultAdapter();
      };
    }
  }

  function fetchDefaultAdapter() {
    var req = bluetoothManager.getDefaultAdapter();
    req.onsuccess = function bt_getAdapterSuccess() {
      if (adapter) {
        adapter.onpbapconnectionreq = null;
        adapter.onobexpasswordreq = null;
        adapter.onpullphonebookreq = null;
        adapter.onpullvcardentryreq = null;
        adapter.onpullvcardlistingreq = null;
      }

      adapter = req.result;
      bindPbapRq();
    };
    req.onerror = function bt_getAdapterFailed() {
      console.error('PBAP', 'ERROR adapter');
    };
  }

  function bindPbapRq() {
    if (adapter) {
      adapter.onpbapconnectionreq = getConfirm.bind(this);
    }
  }


  function pbapInit() {
    pb = new PbapPhonebook();
    if (!adapter) {
      return;
    }

    adapter.onobexpasswordreq = obexPassword.bind(this);
    adapter.onpullphonebookreq = pullPhonebook.bind(this);
    adapter.onpullvcardentryreq = pullVcardEntry.bind(this);
    adapter.onpullvcardlistingreq = pullVcardListing.bind(this);

  }


  function obexPassword(evt) {
    function passwordDialog(text) {
      var responseValues = ['responseTitle', 'initialValue'];
      Promise.all(responseValues.map(key => navigator.mozL10n.formatValue(key)))
      .then(values => {

        var option = {
          type: PbmapIAC.OBEX_PASSWORD,
          header: values[0],
          message: text,
          initialValue: values[1]
        };

        PbmapIAC.callSystemDialog(option).then(result => {
          if (result.value) {
            evt.handle.setPassword(result.password);
          } else {
            evt.handle.reject();
          }
        });
      });
    }

    navigator.mozL10n
      .formatValue('responseMsg', {deviceId: pairedName}).then(text => {
      passwordDialog(text);
    });
  }


  function getConfirm(evt) {
    function callSystemConfirm(text) {
      navigator.mozL10n.formatValue('confirmTitle').then(title => {
        var option = {
          type: PbmapIAC.USER_CONFIRMATION,
          header: title,
          message: text,
          profile: 'PBAP'
        };
        PbmapIAC.callSystemDialog(option).then(result => {
          if (result) {
            evt.handle.accept();
            pbapInit();
          } else {
            evt.handle.reject();
          }
        }).catch(err => {
          console.error(err);
          evt.handle.reject();
        });
      });
    }

    var getNamePromise = new Promise((resolve, reject) => {
      var req = adapter.getPairedDevices();
      if (isBtV2()) {
        req.forEach((item, index, array) => {
          if (item.address === evt.address) {
            if (item.name) {
              resolve(item.name);
            } else {
              // listen for name update.
              item.onattributechanged = function (evt) {
                for (var i in evt.attrs) {
                  if (evt.attrs[i] === 'name') {
                    resolve(item.name);
                    // clean event handler
                    item.onattributechanged = null;
                  }
                }
              };
              //set a timeout in case the device has no name
              setTimeout(()=> {
                // clean event handler
                item.onattributechanged = null;

                // return address instead
                resolve(evt.address);
              }, 2000);
            }
          } else {
            if (index === (array.length - 1)) {
              // return address instead
              resolve(evt.address);
            }
          }
        });
      } else {
        req.onsuccess = function () {
          this.result.forEach((item, index, array) => {
            if (item.address === evt.address) {
              resolve(item.name);
            } else {
              if (index === (array.length - 1)) {
                resolve(evt.address);
              }
            }
          });
        };
        req.onerror = function () {
          reject('failed to get the adapter');
        };
      }
    });

    getNamePromise.then(name => {
      pairedName = name;
      return pairedName;
    }).then(nameText => {
      callSystemConfirm(nameText);
    });
  }


  function pullPhonebook(evt) {
    pb.pullPhoneBook(evt).then((contacts) => {
      if (evt.maxListCount === 0) {
        let vcardBlob = new Blob([], { type : 'text/x-vcard' });
        evt.handle.replyToPhonebookPulling(vcardBlob, contacts.length);
      } else {
        if (contacts.length === 0) {
          let vcardBlob = new Blob([], { type : 'text/x-vcard' });
          evt.handle.replyToPhonebookPulling(vcardBlob, contacts.length);
        } else {
          ContactToVcardBlob(contacts, function blobReady(vcardBlob) {
              evt.handle.replyToPhonebookPulling(vcardBlob, contacts.length);
            },
            {
              // Some MMS gateways prefer this MIME type for vcards
              type: 'text/x-vcard'
            }
          );
        }
      }
    });
  }

  function pullVcardEntry(evt) {
    pb.pullVcardEntry(evt).then((contacts) => {
      if (contacts.length === 0) {
        var vcardBlob = new Blob([], { type: 'text/x-vcard' });
        evt.handle.replyToPhonebookPulling(vcardBlob, contacts.length);
      } else {
        ContactToVcardBlob(contacts, function blobReady(vcardBlob) {
          evt.handle.replyTovCardPulling(vcardBlob, contacts.length);
        }, {
          // Some MMS gateways prefer this MIME type for vcards
          type: 'text/x-vcard'
        });
      }
    });
  }

  function pullVcardListing(evt) {
    pb.pullVcardListing(evt).then((content) => {
      let blob;
      if (evt.maxListCount === 0) {
        blob = new Blob([], { type: 'text/xml' });
      } else {
        blob = new Blob([content.xml], {
          type: 'text/xml'
        });
      }

      evt.handle.replyTovCardListing(blob, content.size);
    });
  }

  function watchMozBluetoothAttributechanged() {
    bluetoothManager.addEventListener('attributechanged', (evt) => {
      for (var i in evt.attrs) {
        switch (evt.attrs[i]) {
          case 'defaultAdapter':
            initDefaultAdapter();
            break;
          default:
            break;
        }
      }
    });
  }

  function initDefaultAdapter() {
    if (adapter) {
      adapter.onpbapconnectionreq = null;
      adapter.onobexpasswordreq = null;
      adapter.onpullphonebookreq = null;
      adapter.onpullvcardentryreq = null;
      adapter.onpullvcardlistingreq = null;
    }
    adapter = bluetoothManager.defaultAdapter;
    bindPbapRq();
  }

  init();
})(window);
