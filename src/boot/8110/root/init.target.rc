# Copyright (c) 2013-2014, The Linux Foundation. All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above
#       copyright notice, this list of conditions and the following
#       disclaimer in the documentation and/or other materials provided
#       with the distribution.
#     * Neither the name of The Linux Foundation nor the names of its
#       contributors may be used to endorse or promote products derived
#       from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
# WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
# ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
# BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
# BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
# OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
# IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
#

#bootmode values: [ unknown ffbm-01 ].
import init.bootmode.${ro.bootmode}.rc

import init.carrier.rc

on early-init
    mkdir /firmware 0771 system system
    mkdir /system 0777 root root
    symlink /data/tombstones /tombstones
    mkdir /carrier 0777 root root
    mkdir /custpack 0777 root root
    mkdir /tctpersist 0500 root root
    write /sys/kernel/mm/ksm/pages_to_scan 100
    write /sys/kernel/mm/ksm/sleep_millisecs 500
    write /sys/kernel/mm/ksm/run 1

on post-fs
    mkdir /persist/smlfile 0777 root system


import init.emmc.${ro.boot.emmc}.rc

on post-fs-data
    mkdir /data/tombstones 0771 system system
    mkdir /data/data/simlockbackup 0777 system system
    mkdir /tombstones/modem 0771 system system
    mkdir /tombstones/lpass 0771 system system
    mkdir /tombstones/wcnss 0771 system system
    mkdir /tombstones/dsps 0771 system system
    mkdir /carrier/chameleon 0771 system system    
    mkdir /persist/data/sfs 0700 system system
    mkdir /persist/data/tz 0700 system system
    mkdir /data/misc/dts 0770 media audio

#start camera server as daemon
service qcamerasvr /system/bin/mm-qcamera-daemon
    class late_start
    user camera
    group camera system inet input graphics

#enable zram
on property:ro.config.zram=true
    swapon_all fstab.qcom
    write /proc/sys/vm/swappiness 100
    write /proc/sys/vm/page-cluster 0

# Allow usb charging to be disabled peristently
on property:persist.usb.chgdisabled=1
    write /sys/class/power_supply/battery/charging_enabled 0

on property:persist.usb.chgdisabled=0
    write /sys/class/power_supply/battery/charging_enabled 1

service qrngd /system/bin/qrngd -f
   class main
   user root
   group root

service qrngp /system/bin/qrngp -f
   class main
   user root
   group root
   oneshot
   disabled

on property:sys.boot_completed=1
   start qrngp

service qseecomd /system/bin/qseecomd
   class core
   user root
   group root

service perfd /system/vendor/bin/perfd
   class main
   user root
   disabled

service thermal-engine /system/vendor/bin/thermal-engine
   class main
   user root
   socket thermal-send-client stream 0666 system system
   socket thermal-recv-client stream 0660 system system
   socket thermal-recv-passive-client stream 0666 system system
   group root

service time_daemon /system/bin/time_daemon
   class late_start
   user root
   group root

service audiod /system/bin/audiod
   class late_start
   user system
   group system

on charger
   wait /dev/block/bootdevice/by-name/system
   mount ext4 /dev/block/bootdevice/by-name/system /system ro barrier=1
   chown root system /sys/class/power_supply/bms/current_now
   chown root system /sys/class/power_supply/bms/voltage_ocv
   chmod 0664 /sys/class/power_supply/bms/current_now
   chmod 0664 /sys/class/power_supply/bms/voltage_ocv
   start vm_bms
   load_all_props
   start qcom-post-boot
   setprop persist.sys.usb.config diag,serial_smd,rmnet_qti_bam,adb

on boot
    insmod /system/lib/modules/adsprpc.ko

service wcnss-service /system/bin/wcnss_service
    class main
    user system
    group system wifi radio
    oneshot

service trace_util /system/bin/trace_util
   class late_start
   user root
   group root
   oneshot

service ppd /system/bin/mm-pp-daemon
    class late_start
    disabled
    user system
    socket pps stream 0660 system system
    group system graphics

service per_mgr /system/bin/pm-service
    class core
    user system
    group system net_raw

#T2M, no surfaceflinger in kaios, but ffbm.
#on property:init.svc.surfaceflinger=stopped
#    stop ppd
#
#on property:init.svc.surfaceflinger=running
#    start ppd

service imsstarter /system/bin/sh /system/etc/init.qti.ims.sh
    class main
    oneshot

on property:vold.decrypt=trigger_restart_framework
    start imsstarter

service imsqmidaemon /system/bin/imsqmidaemon
    class main
    user system
    socket ims_qmid stream 0660 system radio
    group radio net_raw log qcom_diag
    disabled

#Add by zhaoyang.peng for test volte code {
on property:ro.operator.name=TMO
    setprop persist.data.iwlan.enable true
    setprop persist.dbg.volte_avail_ovr 1 
    setprop persist.dbg.vt_avail_ovr 1
    setprop persist.dbg.wfc_avail_ovr 1
    setprop persist.ims.disabled 0
    setprop persist.ims.disabled=0

on property:persist.ims.disabled=0
    sh /etc/init.qti.ims.sh
#Add by zhaoyang.peng for test volte code }

on property:service.qti.ims.enabled=1
    start imsqmidaemon
    start imsdatadaemon

service imsdatadaemon /system/bin/imsdatadaemon
    class main
    user system
    socket ims_datad stream 0660 system radio
    group system wifi radio inet net_raw log qcom_diag net_admin
    disabled

on property:sys.ims.QMI_DAEMON_STATUS=1
    start imsdatadaemon

service ims_rtp_daemon /system/bin/ims_rtp_daemon
   class main
   user system
   socket ims_rtpd stream 0660 system radio
   group radio net_raw diag qcom_diag log inet
   disabled

service imscmservice /system/bin/imscmservice
   class main
   user system
   group radio net_raw diag qcom_diag log
   disabled

on property:sys.ims.DATA_DAEMON_STATUS=1
    start ims_rtp_daemon
    start imscmservice

service touch /system/bin/sh /system/etc/init.qti.synaptics_dsx_qhd.sh
   class late_start
   user root
   group root
   oneshot
   disabled
service dts_configurator /system/bin/dts_configurator
   class main
   user system
   group system
   oneshot

on property:ro.boot.panelname=hx8394d_qhd_video
   start touch

on property:ro.boot.panelname=hx8394d_wvga_video
   start touch

on property:ro.boot.panelname=hx8394d_hvga_video
   start touch

on property:ro.build.type=user
   chmod 0640 /carrier/chameleon.zip

# service fih_fota /system/bin/nvdiag_client -r -p /nvm/num/2497
#    user root
#    group root
#    disabled
#    oneshot

service nvdiag_daemon /system/bin/nvdiag_daemon
    class late_start
    user root
    group root
#the same as the above
#service nvdiag_daemon /system/bin/nvdiag_daemon
#   class late_start
#   user root
#   group root

#init diag_get module
service diag_get_daemon /system/bin/diag_get_daemon
   class late_start
   user root
   group root

#service restore_sfs /system/bin/restore_sfs
#    class late_start
#    oneshot

#service restore_efs /system/bin/restore_efs
#    class late_start
#    oneshot

#T2M, add nv restore after fota
service restore_sfs /system/bin/restore_sfs
    class late_start
    user root
    group root
    oneshot

service nv_backup /system/bin/backup.sh
    class fota
    disabled
    oneshot
#init diag_get module
service diag_get_daemon /system/bin/diag_get_daemon
   class late_start
   user root
   group root

# huanglin add this for start slate_diag command 
service slate_diag /system/bin/slate_diag
    class slate_test
    user system
    group system
    socket slate_diag stream 0666 root root
    disabled
    oneshot
on property:service.slate_diag.enable=1
	start slate_diag
on property:service.slate_diag.enable=0
    stop slate_diag

#[FEATURE]-Add-BEGIN by TCTSH.lin.huang, 2017/02/22, Upgrade traceability version for sugar & otu tools
service up_ver /system/bin/up_ver
    class late_start
    oneshot
    seclabel u:r:up_ver:s0
#[FEATURE]-Add-END by TCTSH.lin.huang

#[FEATURE]-Add-BEGIN by T2M.xiaomanwang.
service tct_diag /system/bin/tct_diag
    class late_start
    user root
    group root
#[FEATURE]-Add-END by T2M.xiaomanwang

service tctd /system/bin/tctd
    class core
    socket tctd stream 0666 root root
